$(document).ready(function (event) {

    $('.card').on('mouseover', function () {
        $(this).children('.meal-details-overlay').removeClass('d-none');
        $(this).children('.meal-details-content').removeClass('d-none');
    });

    $('.card').on('mouseleave', function (event) {
        $(this).children('.meal-details-overlay').addClass('d-none');
        $(this).children('.meal-details-content').addClass('d-none');
    });

    function regular_map() {
        var var_location = new google.maps.LatLng(40.725118, -73.997699);
        var var_mapoptions = {
            center: var_location,
            zoom: 14
        };
    
        var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);
    
        var var_marker = new google.maps.Marker({
            position: var_location,
            map: var_map,
            title: "New York"
        });
    
    };

    google.maps.event.addDomListener(window, 'load', regular_map);

})